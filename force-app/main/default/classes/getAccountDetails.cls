public class getAccountDetails {
@AuraEnabled
    public static List <account> AccountDetails(Id accId){
     
     List<account> AccountDetails = new List <account>();
     System.debug('I got ID' + accId);
     AccountDetails=[SELECT Id, Name, (SELECT AccountId, Name, Email, HomePhone,CreatedDate  FROM Contacts where AccountId = :accId) FROM Account WHERE Id = :accId];
     System.debug('contacts are:' + AccountDetails);
     return AccountDetails;
    }
}