public class getContactList {
    @AuraEnabled
    public static List <contact> ContactList(Id accId){
     
     List<contact> ContactList = new List <contact>();
     System.debug('I got ID' + accId);
     ContactList=[SELECT Id, FirstName,LastName,Email FROM Contact WHERE AccountId=:accId];
     System.debug('contacts are:' + ContactList);
     return ContactList;
    }
    @AuraEnabled
    public static List<contact>  saveContact(Contact Contact,Id accId) {
        List<contact> ContactList =new List<contact>();
         System.debug('I got Contact' + Contact+'Id IS' + accId);
         Contact cont = new Contact(FirstName=Contact.FirstName,LastName=Contact.LastName, Email=Contact.Email, AccountId=accId);
        try{
                System.debug('cont->'+cont);
                if(cont!=null)
                    {
                        insert cont;    
                    } 
               ContactList=[SELECT Id, FirstName,LastName,Email FROM Contact WHERE AccountId=:accId]; 
               System.debug('Contact' + Contact);
          }
        catch(Exception e){
            system.debug('The following exception has occured in saveContact '+e.getLineNumber()+'['+e.getMessage()+']');
            return ContactList; 
        }
           return ContactList;   
    }

}