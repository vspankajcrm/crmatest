({   
    lstContact : function(component, event, helper ,accountId) {
        console.log("Helper"+accountId);
        var action = component.get("c.ContactList");
         action.setParams(
            {
                accId : accountId
            }
        );
        action.setCallback(this, function(response) {
            var state = response.getState();
                 console.log("State"+state); 
             if (state === "SUCCESS") {
                 var result = response.getReturnValue();
                 var jsonObj = JSON.stringify(result);
                 
                 console.log(jsonObj); 
            }
            component.set("v.Contacts",result);
           
        });
        $A.enqueueAction(action);
 
    },
    
     createContact: function(component,newContact,accountId) {
        console.log('inside helper'+ JSON.stringify(newContact));
        
        var action = component.get("c.saveContact");
        console.log('after method called ');
        action.setParams(
         {
             Contact: newContact,
             accId : accountId
         }
        );
        action.setCallback(this, function(response){
        var state = response.getState();
            console.log('Status'+state);
        if (state === "SUCCESS") {
            console.log("HIIIIIIII");
            component.set("v.Contacts",response.getReturnValue());
            //$A.get('e.force:refreshView').fire();
            ifSuccess();
            clearText();
            
        }
            
        function clearText(){
            component.find('fname').set('v.value ', "");
            component.find('lname').set('v.value ', "");
            component.find('email').set('v.value ', "");
        }
            
        function ifSuccess(){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
            title : 'Success',
            message: 'Contact added Successfully',
            duration:' 5000',
            key: 'info_alt',
            type: 'success',
            mode: 'pester'
        });
        toastEvent.fire();
        component.set("v.ShowTable", true);
        component.set("v.ShowForm", false);   
        }
    });
    $A.enqueueAction(action);
},
    
    
})