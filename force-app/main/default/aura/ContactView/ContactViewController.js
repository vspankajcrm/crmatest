({
      handleFilesChange: function (cmp, event) {
      var toggleVal = cmp.find('inputToggle').get('v.value');
      var isChecked = cmp.find('inputToggle').get('v.checked');
      var toggleFrame = cmp.find("toggleFrame");
          if(isChecked==true){
              console.log("T");
              $A.util.addClass(toggleFrame,'slds-flip--horizontal');
          }
          else{
              console.log("s");
              toggleFrame.classList.remove("slds-flip--horizontal");
          }
    },
   HideMe: function(component, event, helper) {
      component.set("v.ShowModule", false);
   },
    
   doInit: function(component, event, helper) {
       var accountId = component.get("v.recordId");
       console.log("Controller"+accountId);
       helper.lstContact(component, event, helper ,accountId);
   },
    
    ShowForm:function(component, event, helper) {
         component.set("v.ShowTable", false);
         component.set("v.ShowForm", true);
   },
    
    backTotable:function(component, event, helper) {
         component.set("v.ShowTable", true);
         component.set("v.ShowForm", false);
   },
    
    clickCreateContact: function(component, event, helper) {
        var accountId = component.get("v.recordId");
        console.log('inside click create contact');
        var newContact = component.get("v.newContact"); 
        helper.createContact(component, newContact,accountId);   
    },
})